/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.utils;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.ListSelectionModel;
import nathan.dungeon_manager.ui.ImageCellRenderer;
import nathan.dungeon_manager.ui.Renderable;

/**
 *
 * @author Nathan
 */
public class InputUtils {

    /**
     * Reads a string from the user.
     *
     * @param parent
     * @param message
     * @param title
     * @return
     */
    public static String inputString(Component parent, String message, String title) {
        return JOptionPane.showInputDialog(parent, message, title, JOptionPane.QUESTION_MESSAGE);
    }

    /**
     * Reads a selection on a list by the user.
     *
     * @param <E>
     * @param owner
     * @param list
     * @param title
     * @param multipleSelection
     * @return
     */
    public static <E extends Renderable> List<E> inputSelection(JFrame owner, E[] list, String title, boolean multipleSelection) {
        JList<E> jlist = new JList<>(list);
        jlist.setCellRenderer(new ImageCellRenderer<>());
        jlist.setSelectionMode(multipleSelection ? ListSelectionModel.MULTIPLE_INTERVAL_SELECTION : ListSelectionModel.SINGLE_SELECTION);
        JDialog dialog = new JDialog(owner);
        dialog.setModal(true);
        dialog.setTitle(title);
        JButton confirm = new JButton("Select");
        confirm.addActionListener((e) -> dialog.dispose());
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(jlist, BorderLayout.CENTER);
        panel.add(confirm, BorderLayout.SOUTH);
        dialog.add(panel);
        dialog.setLocationRelativeTo(owner);
        dialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                jlist.clearSelection();
                super.windowClosing(e);
            }
        });
        dialog.pack();
        dialog.setVisible(true);
        return jlist.getSelectedValuesList();
    }

    /**
     * Displays a message to the user
     *
     * @param parent
     * @param message
     * @param title
     */
    public static void showMessage(Component parent, String message, String title) {
        JOptionPane.showMessageDialog(parent, message, title, JOptionPane.WARNING_MESSAGE);
    }

    /**
     * Displays an about dialog.
     *
     * @param owner
     */
    public static void showAboutDialog(JFrame owner) {
        JDialog dialog = new JDialog(owner);
        dialog.setModal(true);
        dialog.setTitle("About");
        JPanel panel = new JPanel(new BorderLayout());
        JButton closeBtn = new JButton("Close");
        closeBtn.addActionListener((e) -> {
            dialog.dispose();
        });
        panel.add(closeBtn, BorderLayout.SOUTH);
        JLabel label = new JLabel("<html><h1><br />Software for the management of a RPG session.<br />"
                + "Created by Nathan Oliveira (oliveiranathan &lt;at&gt; gmail &lt;dot&gt; com)<br />"
                + "Source code is available at <a href='https://bitbucket.org/oliveiranathan/dungeon-manager'>https://bitbucket.org/oliveiranathan/dungeon-manager</a><br />&nbsp;</h1>");
        panel.add(label, BorderLayout.CENTER);
        dialog.add(panel);
        dialog.setLocationRelativeTo(owner);
        dialog.pack();
        dialog.setVisible(true);
    }

    private InputUtils() {
    }

}
