/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.utils;

import java.util.Random;
import org.mariuszgromada.math.mxparser.Expression;

/**
 *
 * @author Nathan
 */
public class RPGMathUtils {

    /**
     * Calculates an expression with a value and a dice roll.
     *
     * @param life
     * @return
     */
    public static Integer calculateLife(String life) {
        if (life == null) {
            return 0;
        }
        String[] tokens = life.split("\\+");
        int base = Integer.parseInt(tokens[0]);
        if (tokens.length > 1) {
            String[] dice = tokens[1].split("d");
            int number = Integer.parseInt(dice[0]);
            int faces = Integer.parseInt(dice[1]);
            Random r = new Random();
            for (int i = 0; i < number; i++) {
                base += r.nextInt(faces) + 1;
            }
        }
        return base;
    }

    /**
     * Calculates an expression with the basic algebraic stuff...
     *
     * @param expression
     * @return
     */
    public static Integer calculateExpression(String expression) {
        Expression exp = new Expression(expression);
        double result = exp.calculate();
        if (Double.isNaN(result)) {
            return 0;
        }
        return (int) Math.round(result);
    }

    private RPGMathUtils() {
    }

}
