/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.ui.panels;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.geom.AffineTransform;
import java.awt.geom.NoninvertibleTransformException;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import nathan.dungeon_manager.data.map.Map;
import nathan.dungeon_manager.data.monster.Monster;
import nathan.dungeon_manager.data.monster.MonsterRepository;
import nathan.dungeon_manager.data.player.Player;
import nathan.dungeon_manager.data.player.PlayerRepository;
import nathan.dungeon_manager.data.token.MonsterToken;
import nathan.dungeon_manager.data.token.PlayerToken;
import nathan.dungeon_manager.data.token.Token;
import nathan.dungeon_manager.data.token.TokenRepository;
import nathan.dungeon_manager.ui.PlayersWindow;
import nathan.dungeon_manager.utils.InputUtils;
import nathan.dungeon_manager.utils.RPGMathUtils;

/**
 *
 * @author Nathan
 */
public class MapPanel extends JPanel implements Observer {

    private static final long serialVersionUID = 1L;

    private final Map map;
    private final JPanel imagePanel;

    private final State state;
    private final boolean dm;
    private MapPanel playerScreen = null;
    private JPopupMenu pmEmpty;
    private JPopupMenu pmToken;
    private final MapPanel myself = this;

    /**
     * Creates a panel for the display of a map and its tokens.
     *
     * @param map
     * @param playersWindow
     */
    @SuppressWarnings({"OverridableMethodCallInConstructor", "LeakingThisInConstructor"})
    public MapPanel(Map map, PlayersWindow playersWindow) {
        super(new BorderLayout());
        dm = true;
        state = new State();
        state.addObserver(this);
        this.map = map;
        imagePanel = createImagePanel();
        add(imagePanel, BorderLayout.CENTER);
        if (playersWindow != null) {
            JButton btn = new JButton("Send to players' screen.");
            btn.addActionListener((e) -> {
                playerScreen = new MapPanel(map, state);
                playersWindow.setPanel(playerScreen);
            });
            add(btn, BorderLayout.SOUTH);
        }
        calcPositions();
        createPM();
    }

    @SuppressWarnings({"OverridableMethodCallInConstructor", "LeakingThisInConstructor"})
    private MapPanel(Map map, State state) {
        super(new BorderLayout());
        dm = false;
        this.state = state;
        state.addObserver(this);
        this.map = map;
        imagePanel = createImagePanel();
        add(imagePanel, BorderLayout.CENTER);
        calcPositions();
        createPM();
    }

    /**
     * Creates the initiative table panel
     *
     * @return
     */
    public JPanel createInitiativeTable() {
        InitiativePanel ip = new InitiativePanel(state);
        return ip;
    }

    private void createPM() {
        pmEmpty = new JPopupMenu();
        JMenuItem mi = new JMenuItem("Add player");
        mi.addActionListener((evt) -> {
            List<Player> players = PlayerRepository.getPlayers();
            List<Player> selectedPlayers = InputUtils.inputSelection(null, players.toArray(new Player[players.size()]), "Select images to load", false);
            if (!selectedPlayers.isEmpty()) {
                PlayerToken playerToken = new PlayerToken();
                playerToken.setConditions("");
                playerToken.setInitiative(null);
                playerToken.setMap(map);
                playerToken.setPlayer(selectedPlayers.get(0));
                playerToken.setX(state.getI());
                playerToken.setY(state.getJ());
                state.addToken(state.getI(), state.getJ(), playerToken);
                state.triggerNotification();
            }
        });
        pmEmpty.add(mi);
        mi = new JMenuItem("Add monster");
        mi.addActionListener((evt) -> {
            List<Monster> monsters = MonsterRepository.getMonsters();
            List<Monster> selectedMonsters = InputUtils.inputSelection(null, monsters.toArray(new Monster[monsters.size()]), "Select images to load", false);
            if (!selectedMonsters.isEmpty()) {
                MonsterToken monsterToken = new MonsterToken();
                monsterToken.setConditions("");
                monsterToken.setInitiative(null);
                monsterToken.setLife(RPGMathUtils.calculateLife(selectedMonsters.get(0).getLife()));
                monsterToken.setMap(map);
                monsterToken.setMonster(selectedMonsters.get(0));
                monsterToken.setX(state.getI());
                monsterToken.setY(state.getJ());
                state.addToken(state.getI(), state.getJ(), monsterToken);
                state.triggerNotification();
            }
        });
        pmEmpty.add(mi);
        pmToken = new JPopupMenu();
        mi = new JMenuItem("Remove");
        mi.addActionListener((evt) -> {
            if (state.hasToken(state.getI(), state.getJ())) {
                Token token = state.getToken(state.getI(), state.getJ());
                state.removeToken(token);
                state.triggerNotification();
            }
        });
        pmToken.add(mi);
    }

    private void calcPositions() {
        if (!dm) {
            return;
        }
        int panelWidth = getWidth();
        int panelHeight = getHeight();
        BufferedImage img = map.getImage();
        int imgWidth = img.getWidth();
        int imgHeight = img.getHeight();
        double propW = (double) panelWidth / imgWidth;
        double propH = (double) panelHeight / imgHeight;
        double problemProp = Math.min(propW, propH);
        int drawWidth = (int) (imgWidth * problemProp);
        int drawHeight = (int) (imgHeight * problemProp);
        int dW = (panelWidth - drawWidth) / 2;
        int dH = (panelHeight - drawHeight) / 2;
        state.setX(dW);
        state.setY(dH);
        state.setW(drawWidth);
        state.setH(drawHeight);
        state.setProp(problemProp);
        int sqX = imgWidth / map.getGridSize() + 1;
        int sqY = imgHeight / map.getGridSize() + 1;
        state.createTokensIfNotCreated(sqX, sqY);
        state.triggerNotification();
    }

    private AffineTransform createTransform() {
        AffineTransform at = new AffineTransform();
        at.translate(state.getDx(), state.getDy());
        at.scale(state.getZoom(), state.getZoom());
        return at;
    }

    private JPanel createImagePanel() {
        JPanel panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            protected void paintComponent(Graphics g) {
                if (!state.isInitialized()) {
                    calcPositions();
                    state.setInitialized(true);
                }
                super.paintComponent(g);
                AffineTransform transform = ((Graphics2D) g).getTransform();
                g.translate(state.getDx(), state.getDy());
                ((Graphics2D) g).scale(state.getZoom(), state.getZoom());
                g.drawImage(map.getImage(), state.getX(), state.getY(), state.getW(), state.getH(), null);
                int size = (int) (map.getGridSize() * state.getProp());
                int basicWidth = size / 15;
                if (basicWidth < 1) {
                    basicWidth = 1;
                }
                g.setColor(Color.RED);
                g.setFont(new Font(Font.SANS_SERIF, Font.BOLD, (int) (size * 0.3)));
                for (int i = 0; i < state.getGridWidth(); i++) {
                    for (int j = 0; j < state.getGridHeight(); j++) {
                        if (state.getToken(i, j) != null && state.getToken(i, j) != state.getDragged()) {// compare the references, == is correct
                            g.drawImage(state.getToken(i, j).getImage(), i * size + state.getX(), j * size + state.getY(), size, size, null);
                            if (dm && state.getToken(i, j) instanceof MonsterToken) {
                                g.drawString("" + state.getToken(i, j).getHP(), i * size + state.getX(), (int) (j * size + state.getY() + size * 0.95));
                            }
                        }
                    }
                }
                g.setColor(Color.BLACK);
                ((Graphics2D) g).setStroke(new BasicStroke(basicWidth));
                for (int i = state.getX(); i <= state.getX() + state.getW(); i += size) {
                    g.drawLine(i, state.getY(), i, state.getY() + state.getH());
                }
                for (int j = state.getY(); j <= state.getY() + state.getH(); j += size) {
                    g.drawLine(state.getX(), j, state.getX() + state.getW(), j);
                }
                if (state.getSelected() != null && state.getSelected() != state.getDragged()) { // compare the references, == is correct
                    g.setColor(Color.GREEN);
                    ((Graphics2D) g).setStroke(new BasicStroke(2 * basicWidth));
                    g.drawRect(state.getSelected().getX() * size + state.getX(), state.getSelected().getY() * size + state.getY(), size, size);
                }
                ((Graphics2D) g).setTransform(transform);
                if (state.getDragged() != null) {
                    g.setColor(Color.GREEN);
                    g.drawImage(state.getDragged().getImage(), state.getMouseX(), state.getMouseY(), size, size, null);
                    ((Graphics2D) g).setStroke(new BasicStroke(2 * basicWidth));
                    g.drawRect(state.getMouseX(), state.getMouseY(), size, size);
                }
            }

        };
        panel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                calcPositions();
            }

        });
        MouseAdapter ma = createAdapter();
        panel.addMouseMotionListener(ma);
        panel.addMouseListener(ma);
        panel.addMouseWheelListener(ma);
        return panel;
    }

    private MouseAdapter createAdapter() {
        return new MouseAdapter() {

            int x, y;

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                if (!dm) {
                    return;
                }
                super.mouseWheelMoved(e);
                if (e.getWheelRotation() < 0) {
                    state.setZoom(state.getZoom() + state.zoomStep);
                } else {
                    state.setZoom(state.getZoom() - state.zoomStep);
                    if (state.getZoom() < 0.1) {
                        state.setZoom(0.1);
                    }
                }
                state.triggerNotification();
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                x = e.getX();
                y = e.getY();
                popUp(e);
            }

            @Override
            public void mousePressed(MouseEvent e) {
                super.mousePressed(e);
                x = e.getX();
                y = e.getY();
                popUp(e);
                if (SwingUtilities.isLeftMouseButton(e) && state.getI() != -1 && state.getJ() != -1) {
                    if (state.getToken(state.getI(), state.getJ()) != null) {
                        state.setSelected(state.getToken(state.getI(), state.getJ()));
                        if (state.getToken(state.getI(), state.getJ()) instanceof PlayerToken || dm) {
                            state.setDragged(state.getToken(state.getI(), state.getJ()));
                        }
                        state.triggerNotification();
                    }
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                super.mouseReleased(e);
                x = e.getX();
                y = e.getY();
                popUp(e);
                if (SwingUtilities.isLeftMouseButton(e) && state.getI() != -1 && state.getJ() != -1) {
                    if (state.getToken(state.getI(), state.getJ()) != null) { // released on someone that already exists, cant do that
                        state.setDragged(null);
                        state.triggerNotification();
                    } else {
                        state.moveDragged();
                        state.triggerNotification();
                    }
                }
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                super.mouseDragged(e);
                int dx = e.getX() - x;
                int dy = e.getY() - y;
                x = e.getX();
                y = e.getY();
                state.setMouseX(x);
                state.setMouseY(y);
                if (SwingUtilities.isMiddleMouseButton(e) && dm) {
                    state.setDx(state.getDx() + dx);
                    state.setDy(state.getDy() + dy);
                }
                state.triggerNotification();
            }

            private void popUp(MouseEvent e) {
                if (!getIndexes(e)) {
                    return;
                }
                if (SwingUtilities.isRightMouseButton(e) && dm && state.getToken(state.getI(), state.getJ()) == null) {
                    pmEmpty.show(myself, e.getX(), e.getY());
                } else if (SwingUtilities.isRightMouseButton(e) && dm && state.getToken(state.getI(), state.getJ()) != null) {
                    pmToken.show(myself, e.getX(), e.getY());
                }
            }

            private boolean getIndexes(MouseEvent e) {
                try {
                    state.setI(-1);
                    state.setJ(-1);
                    Point2D rawPoint = createTransform().inverseTransform(e.getPoint(), null);
                    int rX = (int) rawPoint.getX();
                    int rY = (int) rawPoint.getY();
                    rX -= state.getX();
                    rY -= state.getY();
                    if (rX < 0 || rY < 0 || rX > state.getW() || rY > state.getH()) {
                        return false;
                    }
                    int size = (int) (map.getGridSize() * state.getProp());
                    rX /= size;
                    rY /= size;
                    if (rX < 0 || rY < 0 || rX >= state.getGridWidth() || rY >= state.getGridHeight()) {
                        return false;
                    }
                    state.setI(rX);
                    state.setJ(rY);
                    return true;
                } catch (NoninvertibleTransformException ex) {
                    Logger.getLogger(MapPanel.class.getName()).log(Level.SEVERE, null, ex);
                    return false;
                }
            }
        };
    }

    /**
     * Adds the list of monsters to the map grid.
     *
     * @param selectedMonsters
     */
    public void addMonsters(List<Monster> selectedMonsters) {
        int i = 0;
        int j = 0;
        for (Monster selectedMonster : selectedMonsters) {
            MonsterToken monsterToken = new MonsterToken();
            monsterToken.setConditions("");
            monsterToken.setInitiative(null);
            monsterToken.setLife(RPGMathUtils.calculateLife(selectedMonster.getLife()));
            monsterToken.setMap(map);
            monsterToken.setMonster(selectedMonster);
            while (state.getToken(i, j) != null) {
                i++;
                if (i >= state.getGridWidth()) {
                    i = 0;
                    j++;
                    if (j >= state.getGridHeight()) {
                        //Can't add any more tokens in the grid...
                        return;
                    }
                }
            }
            monsterToken.setX(i);
            monsterToken.setY(j);
            state.addToken(i, j, monsterToken);
        }
        state.triggerNotification();
    }

    /**
     * Adds the list of players to the map grid.
     *
     * @param selectedPlayers
     */
    public void addPlayers(List<Player> selectedPlayers) {
        int i = 0;
        int j = 0;
        for (Player selectedPlayer : selectedPlayers) {
            PlayerToken playerToken = new PlayerToken();
            playerToken.setConditions("");
            playerToken.setInitiative(null);
            playerToken.setMap(map);
            playerToken.setPlayer(selectedPlayer);
            while (state.getToken(i, j) != null) {
                i++;
                if (i >= state.getGridWidth()) {
                    i = 0;
                    j++;
                    if (j >= state.getGridHeight()) {
                        //Can't add any more tokens in the grid...
                        return;
                    }
                }
            }
            playerToken.setX(i);
            playerToken.setY(j);
            state.addToken(i, j, playerToken);
        }
        state.triggerNotification();
    }

    /**
     * Saves the tokens to the database.
     */
    public void saveTokens() {
        TokenRepository.removeTokensForMap(map);
        state.saveTokens();
    }

    /**
     * Loads the tokens from the database.
     */
    public void loadTokens() {
        List<Token> tokens = TokenRepository.getTokensFor(map);
        tokens.forEach((token) -> {
            state.addToken(token.getX(), token.getY(), token);
        });
        state.sortByInitiative();
        state.triggerNotification();
    }

    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }

}
