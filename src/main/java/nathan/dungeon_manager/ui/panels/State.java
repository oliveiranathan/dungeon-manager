/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.ui.panels;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import lombok.AccessLevel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import nathan.dungeon_manager.data.token.MonsterToken;
import nathan.dungeon_manager.data.token.Token;
import nathan.dungeon_manager.data.token.TokenRepository;

/**
 *
 * @author Nathan
 */
@Data
@EqualsAndHashCode(callSuper = false)
class State extends Observable implements Serializable {

    private static final long serialVersionUID = 1L;

    final double zoomStep = 0.1;
    private int x = 0;
    private int y = 0;
    private int w = 0;
    private int h = 0;
    private double prop = 0;
    private boolean initialized = false;
    private double zoom = 1.0;
    private int dx = 0;
    private int dy = 0;
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private Token[][] tokens = null;
    @Setter(AccessLevel.NONE)
    @Getter(AccessLevel.NONE)
    private List<Token> tokensList = new ArrayList<>(20);
    private int i = 0;
    private int j = 0;
    private int killsXP = 0;
    private int playerCount = 0;
    private int roleplayXP = 0;
    private Token selected;
    private Token dragged;
    private int mouseX;
    private int mouseY;

    void triggerNotification() {
        setChanged();
        notifyObservers();
    }

    boolean addToken(int i, int j, Token token) {
        if (i < 0 || j < 0 || i >= tokens.length || j >= tokens[i].length) {
            return false;
        }
        if (tokens[i][j] == null) {
            tokens[i][j] = token;
            tokensList.add(token);
            return true;
        }
        return false;
    }

    void createTokensIfNotCreated(int x, int y) {
        if (tokens != null) {
            return;
        }
        this.tokens = new Token[x][y];
        tokensList = new ArrayList<>(20);
    }

    void triggerDeath(Token token) {
        if (token instanceof MonsterToken) {
            MonsterToken mt = (MonsterToken) token;
            killsXP += mt.getMonster().getXp();
            tokens[mt.getX()][mt.getY()] = null;
            tokensList.remove(token);
            if (selected == token) {
                selected = null;
            }
            if (dragged == token) {
                dragged = null;
            }
            triggerNotification();
        }
    }

    void sortByInitiative() {
        Collections.sort(tokensList);
    }

    void removeToken(Token token) {
        tokens[token.getX()][token.getY()] = null;
        tokensList.remove(token);
        if (selected == token) {
            selected = null;
        }
        if (dragged == token) {
            dragged = null;
        }
    }

    int getSelectedIndex() {
        return tokensList.indexOf(selected);
    }

    int getTokensCount() {
        return tokensList.size();
    }

    Token getToken(int index) {
        return tokensList.get(index);
    }

    boolean hasToken(int i, int j) {
        return i >= 0 && j >= 0 && i < tokens.length && j < tokens[i].length && tokens[i][j] != null;
    }

    Token getToken(int i, int j) {
        if (!hasToken(i, j)) {
            return null;
        }
        return tokens[i][j];
    }

    void saveTokens() {
        tokensList.forEach((token) -> {
            TokenRepository.addNewToken(token);
        });
    }

    int getGridWidth() {
        return tokens.length;
    }

    int getGridHeight() {
        return tokens[0].length;
    }

    void moveDragged() {
        tokens[dragged.getX()][dragged.getY()] = null;
        dragged.setX(i);
        dragged.setY(j);
        tokens[dragged.getX()][dragged.getY()] = dragged;
        setDragged(null);
    }

}
