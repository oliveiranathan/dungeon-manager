/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.ui.panels;

import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JButton;
import javax.swing.JPanel;
import nathan.dungeon_manager.data.image.Image;
import nathan.dungeon_manager.ui.PlayersWindow;

/**
 *
 * @author Nathan
 */
public class ImagePanel extends JPanel {

    private static final long serialVersionUID = 1L;

    private final Image image;
    private final JPanel imagePanel;

    /**
     * Creates a panel for the display of static images.
     *
     * @param image
     * @param playersWindow
     */
    @SuppressWarnings("OverridableMethodCallInConstructor")
    public ImagePanel(Image image, PlayersWindow playersWindow) {
        super(new BorderLayout());
        this.image = image;
        imagePanel = createImagePanel();
        add(imagePanel, BorderLayout.CENTER);
        if (playersWindow != null) {
            JButton btn = new JButton("Send to players' screen.");
            btn.addActionListener((e) -> playersWindow.setPanel(new ImagePanel(image, null)));
            add(btn, BorderLayout.SOUTH);
        }
    }

    private JPanel createImagePanel() {
        JPanel panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                int panelWidth = getWidth();
                int panelHeight = getHeight();
                BufferedImage img = image.getImage();
                int imgWidth = img.getWidth();
                int imgHeight = img.getHeight();
                double propW = (double) panelWidth / imgWidth;
                double propH = (double) panelHeight / imgHeight;
                double problemProp = Math.min(propW, propH);
                int drawWidth = (int) (imgWidth * problemProp);
                int drawHeight = (int) (imgHeight * problemProp);
                int dW = (panelWidth - drawWidth) / 2;
                int dH = (panelHeight - drawHeight) / 2;
                g.drawImage(img, dW, dH, drawWidth, drawHeight, null);
            }

        };
        return panel;
    }

}
