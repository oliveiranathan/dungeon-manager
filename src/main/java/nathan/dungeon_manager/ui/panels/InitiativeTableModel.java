/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.ui.panels;

import java.util.Observable;
import java.util.Observer;
import javax.swing.table.AbstractTableModel;
import nathan.dungeon_manager.data.token.MonsterToken;
import nathan.dungeon_manager.utils.RPGMathUtils;

/**
 *
 * @author Nathan
 */
public class InitiativeTableModel extends AbstractTableModel implements Observer {

    private static final long serialVersionUID = 1L;
    private static final String[] columnNames = {"Initiative", "Name", "HP", "Conditions"};
    private static final Class<?>[] columnClasses = {Integer.class, String.class, String.class, String.class};

    private final State state;

    @SuppressWarnings("LeakingThisInConstructor")
    InitiativeTableModel(State state) {
        this.state = state;
        state.addObserver(this);
    }

    @Override
    public int getRowCount() {
        return state.getTokensCount();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (columnIndex >= columnNames.length) {
            return "";
        }
        return columnNames[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        if (columnIndex >= columnClasses.length) {
            return Object.class;
        }
        return columnClasses[columnIndex];
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (rowIndex >= state.getTokensCount() || columnIndex >= columnNames.length) {
            return false;
        }
        if (columnIndex == 0 || columnIndex == 3) {
            return true;
        }
        if (columnIndex == 1) {
            return false;
        }
        return state.getToken(rowIndex) instanceof MonsterToken;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex >= state.getTokensCount()) {
            return null;
        }
        switch (columnIndex) {
            case 0://Initiative
                return state.getToken(rowIndex).getInitiative();
            case 1://Name
                return state.getToken(rowIndex).getName();
            case 2://HP
                return state.getToken(rowIndex).getHP();
            case 3://Conditions
                return state.getToken(rowIndex).getConditions();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (rowIndex >= state.getTokensCount()) {
            return;
        }
        switch (columnIndex) {
            case 0://Initiative
                state.getToken(rowIndex).setInitiative((Integer) aValue);
                state.sortByInitiative();
                state.triggerNotification();
                break;
            case 1://Name
                // nop, never changes
                break;
            case 2://HP
                state.getToken(rowIndex).setHP(RPGMathUtils.calculateExpression((String) aValue));
                if (state.getToken(rowIndex).getHP() <= 0) {
                    state.triggerDeath(state.getToken(rowIndex));
                } else {
                    state.triggerNotification();
                }
                break;
            case 3://Conditions
                state.getToken(rowIndex).setConditions((String) aValue);
                state.triggerNotification();
                break;
            default:
                break;
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        fireTableDataChanged();
    }

}
