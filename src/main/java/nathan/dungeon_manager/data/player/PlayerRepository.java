/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.player;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import nathan.dungeon_manager.db.HibernateUtil;
import nathan.dungeon_manager.utils.ImageUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Nathan
 */
public class PlayerRepository {

    /**
     * Saves a new player to the database.
     *
     * @param name
     * @param file
     * @throws IOException
     */
    public static void addNewPlayer(String name, File file) throws IOException {
        Player player = new Player();
        player.setName(name);
        player.setImage(ImageUtils.toByte(ImageIO.read(file)));
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(player);
        tx.commit();
        session.close();
    }

    /**
     * Gets all players from the database.
     *
     * @return
     */
    public static List<Player> getPlayers() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from Player");
        @SuppressWarnings("unchecked")
        List<Player> list = query.list();
        tx.commit();
        session.close();
        return list;
    }

    private PlayerRepository() {
    }

}
