/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.player;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import lombok.Data;
import nathan.dungeon_manager.data.image.Image;
import nathan.dungeon_manager.ui.Renderable;
import nathan.dungeon_manager.utils.ImageUtils;

/**
 *
 * @author Nathan
 */
@Entity
@Data
public class Player implements Serializable, Renderable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    private Long id;

    private String name;

    @Column(length = 2 * 1_024 * 1_024)
    @Lob
    private byte[] image;

    /**
     * Getter
     *
     * @return
     */
    @Override
    public BufferedImage getImage() {
        try {
            return ImageUtils.toImage(image);
        } catch (IOException ex) {
            Logger.getLogger(Image.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /**
     * Setter
     *
     * @param img
     */
    public void setImage(byte[] img) {
        Arrays.copyOf(img, img.length);
    }

    /**
     * Getter
     *
     * @return
     */
    @Override
    public String getText() {
        return name;
    }

}
