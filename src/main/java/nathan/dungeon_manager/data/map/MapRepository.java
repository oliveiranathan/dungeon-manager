/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.map;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import nathan.dungeon_manager.db.HibernateUtil;
import nathan.dungeon_manager.utils.ImageUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Nathan
 */
public class MapRepository {

    /**
     * Saves a new map to the database.
     *
     * @param name
     * @param file
     * @param size
     * @return
     * @throws IOException
     */
    public static Map addNewMap(String name, File file, int size) throws IOException {
        Map map = new Map();
        map.setName(name);
        map.setImage(ImageUtils.toByte(ImageIO.read(file)));
        map.setGridSize(size);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(map);
        tx.commit();
        session.close();
        return map;
    }

    /**
     * Gets the list of maps from the database.
     *
     * @return
     */
    public static List<Map> getMaps() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from Map");
        @SuppressWarnings("unchecked")
        List<Map> list = query.list();
        tx.commit();
        session.close();
        return list;
    }

    private MapRepository() {
    }

}
