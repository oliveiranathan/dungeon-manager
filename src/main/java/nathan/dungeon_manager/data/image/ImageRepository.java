/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.image;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import nathan.dungeon_manager.db.HibernateUtil;
import nathan.dungeon_manager.utils.ImageUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Nathan
 */
public class ImageRepository {

    /**
     * Saves a new image to the database.
     *
     * @param name
     * @param file
     * @return
     * @throws IOException
     */
    public static Image addNewImage(String name, File file) throws IOException {
        Image img = new Image();
        img.setName(name);
        img.setImage(ImageUtils.toByte(ImageIO.read(file)));
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(img);
        tx.commit();
        session.close();
        return img;
    }

    /**
     * Loads a list of images from the database.
     *
     * @return
     */
    public static List<Image> getImages() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from Image");
        @SuppressWarnings("unchecked")
        List<Image> list = query.list();
        tx.commit();
        session.close();
        return list;
    }

    private ImageRepository() {
    }

}
