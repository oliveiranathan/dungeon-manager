/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.monster;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.imageio.ImageIO;
import nathan.dungeon_manager.db.HibernateUtil;
import nathan.dungeon_manager.utils.ImageUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Nathan
 */
public class MonsterRepository {

    /**
     * Saves a new monster to the database.
     *
     * @param name
     * @param file
     * @param life
     * @param xp
     * @throws IOException
     */
    public static void addNewMonster(String name, File file, String life, int xp) throws IOException {
        Monster monster = new Monster();
        monster.setName(name);
        monster.setImage(ImageUtils.toByte(ImageIO.read(file)));
        monster.setLife(life);
        monster.setXp(xp);
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(monster);
        tx.commit();
        session.close();
    }

    /**
     * Gets the list of monsters from the database.
     *
     * @return
     */
    public static List<Monster> getMonsters() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from Monster");
        @SuppressWarnings("unchecked")
        List<Monster> list = query.list();
        tx.commit();
        session.close();
        return list;
    }

    private MonsterRepository() {
    }

}
