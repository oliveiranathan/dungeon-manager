/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.token;

import java.awt.image.BufferedImage;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import lombok.Data;
import nathan.dungeon_manager.data.map.Map;
import nathan.dungeon_manager.data.monster.Monster;

/**
 *
 * @author Nathan
 */
@Entity
@Data
public class MonsterToken implements Serializable, Token {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic
    private Long id;

    @ManyToOne
    private Monster monster;

    @ManyToOne
    private Map map;

    private Integer initiative;

    private Integer life;

    private String conditions;

    private Integer x;

    private Integer y;

    /**
     * Getter
     *
     * @return
     */
    @Override
    public BufferedImage getImage() {
        return monster.getImage();
    }

    /**
     * Getter
     *
     * @return
     */
    @Override
    public String getName() {
        return monster.getName();
    }

    /**
     * Getter
     *
     * @return
     */
    @Override
    public Integer getHP() {
        return life;
    }

    /**
     * Getter
     *
     * @param hp
     */
    @Override
    public void setHP(Integer hp) {
        life = hp;
    }

}
