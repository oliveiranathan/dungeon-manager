/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.token;

import java.util.ArrayList;
import java.util.List;
import nathan.dungeon_manager.data.map.Map;
import nathan.dungeon_manager.db.HibernateUtil;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Nathan
 */
public class TokenRepository {

    /**
     * Saves a new token to the database.
     *
     * @param token
     */
    public static void addNewToken(Token token) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(token);
        tx.commit();
        session.close();
    }

    /**
     * Load the map`s tokens
     *
     * @param map
     * @return
     */
    public static List<Token> getTokensFor(Map map) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("from PlayerToken where map=:map");
        query.setEntity("map", map);
        @SuppressWarnings("unchecked")
        List<PlayerToken> listPlayer = query.list();
        query = session.createQuery("from MonsterToken where map=:map");
        query.setEntity("map", map);
        @SuppressWarnings("unchecked")
        List<MonsterToken> listMonster = query.list();
        tx.commit();
        session.close();
        List<Token> list = new ArrayList<>(listPlayer.size() + listMonster.size());
        list.addAll(listPlayer);
        list.addAll(listMonster);
        return list;
    }

    /**
     * Removes all tokens for the given map
     *
     * @param map
     */
    public static void removeTokensForMap(Map map) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        Query query = session.createQuery("delete from PlayerToken where map=:map");
        query.setEntity("map", map);
        query.executeUpdate();
        query = session.createQuery("delete from MonsterToken where map=:map");
        query.setEntity("map", map);
        query.executeUpdate();
        tx.commit();
        session.close();
    }

    private TokenRepository() {
    }

}
