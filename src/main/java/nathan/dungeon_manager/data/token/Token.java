/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nathan.dungeon_manager.data.token;

import java.awt.image.BufferedImage;

/**
 *
 * @author Nathan
 */
@SuppressWarnings("MarkerInterface")
public interface Token extends Comparable<Token> {

    /**
     * Gets the token image representation
     *
     * @return
     */
    BufferedImage getImage();

    /**
     * Gets the token X position on the grid
     *
     * @return
     */
    Integer getX();

    /**
     * Gets the token Y position on the grid
     *
     * @return
     */
    Integer getY();

    /**
     * Gets the initiative for the token
     *
     * @return
     */
    Integer getInitiative();

    /**
     * Gets the token text representation
     *
     * @return
     */
    String getName();

    /**
     * Gets the token hit points
     *
     * @return
     */
    Integer getHP();

    /**
     * Gets the token conditions
     *
     * @return
     */
    String getConditions();

    /**
     * Sets the initiative for the token
     *
     * @param initiative
     */
    void setInitiative(Integer initiative);

    /**
     * Set the token conditions
     *
     * @param conditions
     */
    void setConditions(String conditions);

    /**
     * Sets the HP for the token
     *
     * @param hp
     */
    void setHP(Integer hp);

    @Override
    default int compareTo(Token o) {
        Integer mine = getInitiative();
        Integer their = o.getInitiative();
        if (mine == null) {
            mine = 0;
        }
        if (their == null) {
            their = 0;
        }
        return their - mine;
    }

    /**
     * Set the token X position
     *
     * @param i
     */
    void setX(Integer i);

    /**
     * Set the token Y position
     *
     * @param i
     */
    void setY(Integer i);
}
